package com.rabat.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

/**
 * Created by user on 03/06/2016.
 */
@Entity
public class University {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idUniversity;
    private String name;
    private String adresse;
    @OneToMany(mappedBy = "university")
    @JsonManagedReference
    private List<Formation> formations;

    public long getIdUniversity() {
        return idUniversity;
    }

    public void setIdUniversity(long idUniversity) {
        this.idUniversity = idUniversity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
}
