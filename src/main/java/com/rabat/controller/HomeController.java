package com.rabat.controller;

import com.rabat.entities.Formation;
import com.rabat.entities.University;
import com.rabat.service.FormationService;
import com.rabat.service.UniversityService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by user on 03/06/2016.
 */
@RestController
public class HomeController {


    @Resource
    private UniversityService universityService;

    @Resource
    private FormationService formationService;

    @RequestMapping("/universities")
    public List<University> university() {
        return universityService.findAll();
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public University addUniversity(@RequestBody University university) {
        return universityService.save(university);
    }

    @RequestMapping("/formation/{idUniversity}")
    public List<Formation> formation(@PathVariable long idUniversity) {
        University university = universityService.findOne(idUniversity);
        return formationService.findByUniversity(university);
    }
}
