package com.rabat.service;

import com.rabat.entities.Formation;
import com.rabat.entities.University;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by user on 03/06/2016.
 */
public interface FormationService  extends CrudRepository<Formation, Long> {
    List<Formation> findByUniversity(University university);
}
