package com.rabat.service;

import com.rabat.entities.University;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by user on 03/06/2016.
 */
public interface UniversityService extends CrudRepository<University, Long> {

    @Override
    List<University> findAll();
}
